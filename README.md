# Conform.js

This is a javascript library used to conform columns responsively.   

## Requirements

* jQuery

## Basic usage    

Columns on the same row will equal heights   

```
<div data-conform-set="example">
  <div data-conform="example"></div>
  <div data-conform="example"></div>
</div>
```  

## Master/Slave example  

Force matching one column height (slave) to another column (master), but not the other way around.  

```
<div data-conform-master="example"></div>
<div data-conform-slave="example"></div>
```

## After images load

You may want to reconform after images load. You can do this by adding `data-conform-images` to your conform container.  

```
<div data-conform-set="example" data-conform-images>
  <div data-conform="example"></div>
  <div data-conform="example"></div>
</div>
```  

## Agnostic source-order conforming

Sometimes you may need to match columns that aren't in source-order. To do this, add the `data-conform-agnostic` attribute to your conform container.  

```
<div data-conform-set="example" data-conform-agnostic>
  <div data-conform="example"></div>
  <div data-conform="example"></div>
</div>
```  

## Triggers

### Conform the columns again for a specific set

`$(".my-columns").trigger("reconform");`

### Conform a master/slave set

`$(".my-master-column").trigger("reconform");`

### Conform all columns on pages

`$(document).trigger("conform-columns")`

### Conform all column sets on page

`$(document).trigger("conform-column-sets")`

### Conform all master/slaves on page

`$(document).trigger("conform-mater-slaves")`

## Changelog

### 1.0

* Initial release

## Notes and acknowledgements

Based on a snippet from css-tricks:  
http://css-tricks.com/equal-height-blocks-in-rows/